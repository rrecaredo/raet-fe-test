'use strict';

const gulp       = require('gulp');
const inject     = require('gulp-inject');
const sourcemaps = require('gulp-sourcemaps');
const buffer     = require('vinyl-buffer');
const browserify = require('browserify');
const babelify   = require('babelify');
const source     = require('vinyl-source-stream');
const concat     = require('gulp-concat');
const lint       = require('gulp-eslint');
const size       = require('gulp-size');
const imagemin   = require('gulp-imagemin');
const cache      = require('gulp-cache');
const bs         = require('browser-sync');
const nodemon    = require('nodemon');

// -----------------------------------
// CONFIGURATION
// -----------------------------------

const config = {
    port : 9005,
    paths: {
        html  : './src/client/*.html',
        js    : './src/client/**/*.js',
        css   : './src/client/**/*.css',
        images: 'src/client/images/**/*.{gif,jpg,png,svg}',
        bs    : [
            'node_modules/bootstrap/dist/css/bootstrap.min.css',
            'node_modules/bootstrap/dist/css/bootstrap-theme.min.css'
        ],
        base  : 'http://localhost',
        dist  : './dist',
        mainJs: './src/client/main.js'
    }
};

// -----------------------------------
// SERVER
// -----------------------------------

gulp.task('nodemon', (cb) => {
    var called = false;
    return nodemon({
        script: 'src/server/api.js',
        ignore: [
            'gulpfile.js',
            'node_modules/'
        ]
    }).on('start', () => {
        if (!called) {
            called = true;
            cb();
        }
    }).on('restart', function () {
        setTimeout(function () {
            bs.reload({stream: false});
        }, 1000);
    });
});

// -----------------------------------
// LIVE RELOAD
// -----------------------------------

gulp.task('browser-sync', ['nodemon'], () => {

    bs({
        proxy : "localhost:3000",
        port  : 5000,
        notify: true
    });
});

gulp.task('reload', bs.reload);

// -----------------------------------
// JS
// -----------------------------------

gulp.task('js', () => {
    browserify(config.paths.mainJs, {debug: true})
        .transform(babelify)
        .bundle()
        .on('error', console.error.bind(console))
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(`${config.paths.dist}/scripts`))
        .pipe(bs.reload({stream: true}));
});

// -----------------------------------
// LINT
// -----------------------------------

gulp.task('lint', () => {
    return gulp.src(config.paths.js)
        .pipe(lint())
        .pipe(lint.format());
});

// -----------------------------------
// CSS
// -----------------------------------

gulp.task('css', () => {
    gulp.src(config.paths.bs)
        .pipe(concat('bundle.css'))
        .pipe(gulp.dest(config.paths.dist));
});

// -----------------------------------
// IMAGES
// -----------------------------------

gulp.task('images', () => {
    return gulp.src([config.paths.images])
        .pipe(cache(imagemin({
            progressive: true,
            interlaced : true,
            pngquant   : true
        })))
        .pipe(gulp.dest(`${config.paths.dist}/images`))
        .pipe(size({title: 'images'}));
});

// -----------------------------------
// HTML
// -----------------------------------

gulp.task('html', ['js', 'css'], () => {
    gulp.src(config.paths.html)
        .pipe(inject(gulp.src([`${config.paths.dist}/**/*.js`, `${config.paths.dist}/**/*.css`], {read: false}), {
            relative  : false,
            ignorePath: 'dist'
        }))
        .pipe(gulp.dest(config.paths.dist))
        .pipe(bs.reload({stream: true}));
});

// -----------------------------------
// WATCH
// -----------------------------------

gulp.task('watch', ['browser-sync'], () => {
    gulp.watch(config.paths.js, ['js', 'lint'])
    gulp.watch(config.paths.css, ['css'])
    gulp.watch(config.paths.images, ['images', 'reload'])
    gulp.watch(config.paths.html, ['html', 'reload'])
});

// -----------------------------------
// DEFAULT
// -----------------------------------

gulp.task('default', ['html', 'images', 'lint', 'watch']);