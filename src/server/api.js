var express    = require('express');
var app        = express();
var port       = 3000;
var apiRouter  = express.Router();
var bodyParser = require('body-parser');
var path       = require('path');

apiRouter.route('/').get((req, res) => {
    res.send('Raet API');
});

apiRouter.route('/:id').get((req, res) => {
    res.send(`Raet API - ${req.params.id}`);
});

apiRouter.route('/').post((req, res) => { });

app.use('/api', apiRouter);

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../../dist/index.html'));
});

app.use('/', express.static('../../dist'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.listen(port, (err) => {
    if (err) {
        console.log(err);
    }
    console.log(`Listening on port ${port}`);
});